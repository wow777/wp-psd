<!--main navigation-->
<div class="main-menu-wrap">
    <nav class="main-menu">
        <ul>
            <li><a href="#" class="nav-dashboard current">dashboard</a></li>
            <li><a href="#" class="nav-quotes">quotes</a></li>
            <li><a href="#" class="nav-renewals">renewals</a></li>
            <li><a href="#" class="nav-clients">clients</a></li>
            <li><a href="#" class="nav-notify">notifications</a></li>
        </ul>
    </nav>
</div>
<!--end of main navigation-->